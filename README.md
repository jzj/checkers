# Checkers

A classic game you can now play in the terminal!

Written by Erik Hunter and John Jekel in 2020. MIT licensed, so you can pretty much hack it all you want.

## Todo

1. Create Engine
2. Create console front end
3. Create gui front end (Qt or GTK)
4. Cross platform build/runtime support
5. Create better readme/documentation
6. Create a better build method (maybe figure out make)

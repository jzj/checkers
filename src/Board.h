#ifndef BOARD_H
#define BOARD_H

#include <vector>
#include <array>
#include <string>

class Board
{
public:
    //defining vectors for easier use later
    typedef std::vector<std::array<unsigned int, 2>> pieceCoordinatesVector_t;//[pieceNum][0] is x, [pieceNum][1] is y
    typedef std::vector<bool> pieceAliveVector_t;
    typedef std::vector<bool> pieceKingedVector_t;
    typedef std::vector<bool> pieceIsP1Vector_t;
    
    typedef std::vector<std::array<unsigned int, 2>> invalidTilesCoordinatesVector_t;
    
    //all piece vector indexes must correspond for the same piece (eg. _pieceCoordinates[0] is for same piece as _pieceIsKinged[0])
    //Also must be contigous
    Board(unsigned int _numOfPieces, pieceCoordinatesVector_t _pieceCoordinates, pieceAliveVector_t _pieceAlive, pieceKingedVector_t _pieceIsKinged, pieceIsP1Vector_t _pieceIsP1, invalidTilesCoordinatesVector_t _invalidTiles, unsigned int _maxX, unsigned int _maxY);
    Board(std::string fileName);
    
    //Piece Management
    unsigned int getXCoordinate(unsigned int pieceNum);
    unsigned int getYCoordinate(unsigned int pieceNum);
    void setXCoordinate(unsigned int pieceNum, unsigned int x);
    void setYCoordinate(unsigned int pieceNum, unsigned int y);
    bool isAlive(unsigned int pieceNum);
    void killPiece(unsigned int pieceNum);//NEVER REMOVES A PIECE FROM THE VECTORS, ONLY SETS PROPER pieceAlive var
    void resurrectPiece(unsigned int pieceNum);
    bool isKing(unsigned int pieceNum);
    void kingPiece(unsigned int pieceNum);
    void dethronePiece(unsigned int pieceNum);
    bool isP1Piece(unsigned int pieceNum);
    
    unsigned int getNumOfPieces();
    
    //OOB functions
    unsigned int getMaxXCoordinate();//note that this is 0 indexed
    unsigned int getMaxYCoordinate();//note that this is 0 indexed
    void setMaxXCoordinate(unsigned int x);//note that this is 0 indexed
    void setMaxYCoordinate(unsigned int y);//note that this is 0 indexed
    invalidTilesCoordinatesVector_t getInvalidTilesVector();
    
    //Board loading/saving
    void fileRead(const std::string &fileName);//auto detects if is template or save file, initilaizes things appropriatly
    void templateWrite(const std::string &fileName);//write board to disk skipping tiles that are dead (template)
    void saveWrite(const std::string &fileName);//write board to disk including dead tiles (save game)
    
private:
    pieceCoordinatesVector_t pieceCoordinates;
    pieceAliveVector_t pieceAlive;
    pieceKingedVector_t pieceIsKinged;
    pieceIsP1Vector_t pieceIsP1;
    
    invalidTilesCoordinatesVector_t invalidTiles;
    unsigned int maxX, maxY;//note that these are 0 indexed
    
    bool isPlayerOneTurn = true;//player one starts by default
    unsigned int numOfPieces;//not 0 indexed; value of 8 means 8 pieces from 0 to 7
    
    //Board loading
    void templateRead(const std::string &fileName);//chosen by fileRead()
    void saveRead(const std::string &fileName);//chosen by fileRead()
    
//old hardcoded interface (only 24 pieces and 64 tiles)
/*public:
    

    //Board(int _pieceCoordinates[24][2], bool _pieceAlive[24], bool _pieceIsKinged[24]);
    
    int getXCoordinate(int pieceNum);
    int getYCoordinate(int pieceNum);
    void setXCoordinate(int pieceNum, int y);
    void setYCoordinate(int pieceNum, int y);
    bool isDeadJim(int pieceNum);
    void killPiece(int pieceNum);
    void resurrectPiece(int pieceNum);
    void kingPiece(int pieceNum);
    void dethronePiece(int pieceNum);

private:
    int pieceCoordinates[24][2];
    bool pieceAlive[24];
    bool pieceIsKinged[24];
    bool isPlayerOneTurn = true;//player one starts by default
*/
};

#endif // BOARD_H

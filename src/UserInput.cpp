
#include <iostream>
#include <sstream>

#include "UserInput.h"
#include "Board.h"

/* Public Methods */
/* Static Methods */

void UserInput::printBoardStats(Board &_board)
{
    //Stats
    std::cout << "Max X: " << _board.getMaxXCoordinate() << std::endl;
    std::cout << "Max Y: " << _board.getMaxYCoordinate() << std::endl;
    std::cout << std::endl;
    
    //Pieces
    const unsigned int numOfPieces = _board.getNumOfPieces();
    std::cout << "Number Of Pieces: " << numOfPieces << std::endl << std::endl;
    
    if (numOfPieces > 0)
    {
        for (int i = 0; i < numOfPieces; ++i)
        {
            std::cout << "Piece: "  << i << std::endl;
            std::cout << "X: " << _board.getXCoordinate(i) << std::endl;
            std::cout << "Y: " << _board.getYCoordinate(i) << std::endl;
            std::cout << "isAlive: " << (_board.isAlive(i) ? "yes" : "no") << std::endl;
            std::cout << "isKing: " << (_board.isKing(i) ? "yes" : "no") << std::endl;
            std::cout << "Owner: " << (_board.isP1Piece(i) ? "Player_1" : "Player_2") << std::endl;
            std::cout << std::endl;
        }
    }
        
    //Invalid Tiles
    Board::invalidTilesCoordinatesVector_t invalidTiles = _board.getInvalidTilesVector();
    const unsigned long numOfInvalidTiles = invalidTiles.size();
    std::cout << "Number Of Invalid Tiles: " << numOfInvalidTiles << std::endl << std::endl;
    
    if (numOfPieces > 0)
    {
        for (int i = 0; i < numOfInvalidTiles; ++i)
        {
            std::cout << "Invalid Tile: " << i << std::endl;
            std::cout << "X: " << invalidTiles[i][0] << std::endl;
            std::cout << "Y: " << invalidTiles[i][1] << std::endl;
            std::cout << std::endl;
        }
    }
}

void UserInput::printBoard(Board &_board)
{
    /*
    5 spaces for left coordinates 
    12345
         ╔═══╦═══╦═══╦═══╦═══╦═══╦═══╦═══╗//Top of board
       7 ║   ║   ║   ║   ║   ║   ║   ║   ║//Tile row
         ╠═══╬═══╬═══╬═══╬═══╬═══╬═══╬═══╣//Intermediate row
       6 ║   ║   ║   ║   ║   ║   ║   ║   ║//Tile row
         ╠═══╬═══╬═══╬═══╬═══╬═══╬═══╬═══╣//Intermediate row
       5 ║   ║   ║   ║   ║   ║   ║   ║   ║//Tile row
         ╠═══╬═══╬═══╬═══╬═══╬═══╬═══╬═══╣//Intermediate row
       4 ║   ║   ║   ║   ║   ║   ║   ║   ║//Tile row
         ╠═══╬═══╬═══╬═══╬═══╬═══╬═══╬═══╣//Intermediate row
       3 ║   ║   ║   ║   ║   ║   ║   ║   ║//Tile row
         ╠═══╬═══╬═══╬═══╬═══╬═══╬═══╬═══╣//Intermediate row
       2 ║   ║   ║   ║   ║   ║   ║   ║   ║//Tile row
         ╠═══╬═══╬═══╬═══╬═══╬═══╬═══╬═══╣//Intermediate row
       1 ║   ║   ║   ║   ║   ║   ║   ║   ║//Tile row
         ╠═══╬═══╬═══╬═══╬═══╬═══╬═══╬═══╣//Intermediate row
       0 ║   ║   ║   ║   ║   ║   ║   ║   ║//Tile row
         ╚═══╩═══╩═══╩═══╩═══╩═══╩═══╩═══╝//Bottom of board
           A   B   C   D   E   F   G   H   //Bottom coordinates (technically spaces after last coord extend past board but you can't see them)
    */
    
    const unsigned int maxX = _board.getMaxXCoordinate();
    const unsigned int maxY = _board.getMaxXCoordinate();
    
    //Print top of board
    std::cout << "     ╔═══";//top corner + first section (╔ + 3 * ═)
    for (int i = 0; i < maxX; ++i)//x coords counting up but since we already did the first section, we only count up to maxX-1
    {
        std::cout << "╦═══";//(╦ + 3 * ═)
    }
    std::cout << "╗" << std::endl;//finish top 
    
    //Print rows
    for (int i = maxY; i >= 0; --i)//y coords counting down
    {
        //Print left coordinates
        int coordToPrint = i + 1;
        
        if (coordToPrint < 10)
            std::cout << "   " << coordToPrint;//3 spaces before + # + 1 space after = 5 spaces
        else if (coordToPrint < 100)
            std::cout << "  " << coordToPrint;//2 spaces before + ## + 1 space after = 5 spaces
        else if (coordToPrint < 1000)
            std::cout << " " << coordToPrint;//1 spaces before + ### + 1 space after = 5 spaces
        else if (coordToPrint < 10000)
            std::cout << coordToPrint;//0 spaces before + #### + 1 space after = 5 spaces
        
        //Print tile row
        std::cout<< " ║ ";//start of row + coordinate
        for (int j = 0; j <= maxX; ++j)//x coords counting up
        {
            int tileCode = tileCodeAt(_board, j, i);//0 if p1, 1 if p2, 2 if p1 king, 3 if p2 king, 4 if invalid tile, -1 means piece is non existent/dead
            switch (tileCode)
            {
                case 0://p1
                {
                    std::cout << "\x1B[91m●";//red ●
                    break;
                }
                case 1://p2
                {
                    std::cout << "\x1B[94m○";//blue ○
                    break;
                }
                case 2://p1 kinged
                {
                    std::cout << "\x1B[91m♚";//red ♚
                    break;
                }
                case 3://p2 kinged
                {
                    std::cout << "\x1B[94m♔";//blue ♔
                    break;
                }
                case 4://invalid tile
                {
                    std::cout << "\x1B[1mX";//bold X
                    break;
                }
                case -1://piece is non existent/dead
                default:
                {
                    std::cout << ' ';//space
                    break;
                }
                
            }
            std::cout << "\x1b[0m";//reset colour
            if (j < maxX)//don't print the inbetween ║ if it is the last tile in the row; let the std::cout 3 lines below end the line
                std::cout << " ║ ";//in between tiles
        }
        std::cout << " ║" << std::endl;//finish tile row
        
        if (i > 0)//don't print an intermediate row below the tile row if the last tile row was just printed; let the bottom of the board be printed instead
        {
            //Print intermediate row
            std::cout << "     ╠═══";//top corner + first section (╠ + 3 * ═)
            for (int i = 0; i < maxX; ++i)//x coords counting up but since we already did the first section, we only count up to maxX-1
            {
                std::cout << "╬═══";//(╬ + 3 * ═)
            }
            std::cout << "╣" << std::endl;//finish intermediate row
        }
    }
    
    //Print bottom of board
    std::cout << "     ╚═══";//top corner + first section (╔ + 3 * ═)
    for (int i = 0; i < maxX; ++i)//x coords counting up but since we already did the first section, we only count up to maxX-1
    {
        std::cout << "╩═══";//(╦ + 3 * ═)
    }
    std::cout << "╝" << std::endl;//finish top
    
    //Print bottom coordinates
    std::cout << "       ";//5 spaces + spaces to align coordinates
    for (int i = 0; i <= maxX; ++i)//x coords counting up but since we already did the first space, we only count up to maxX-1
    {
        char coordLetter = 'A' + i;//increases unicode code
        std::cout << coordLetter << "   ";
    }
    std::cout << std::endl;
}

/* !Static Methods */
UserInput::UserInput(Board &_board) : board(_board)//copy the reference for use by other UserInput non-static functions
{
    //nothing needed here because apparently references (in this case to a Board) need to use a "constructor initialization list" instead of using regular assignment
    //references must always refer to something so we can't spend any time in a function, even the constructor, without all references referencing
    //apparently this is better practice for even regular parameters that don't absolutly need the : thing anyways, so TODO we should change other constructors to use the : thing as well
    //https://isocpp.org/wiki/faq/ctors#init-lists
}

void UserInput::run()
{
    std::string input, temp;
    std::stringstream inputStringStream;
    bool wantsToExit = false;
    
    while (!wantsToExit)
    {
        std::cout << "Checkers\x1B[1;36m>\x1b[0m";//the console prompt
        
        std::getline(std::cin, input);
        inputStringStream = std::stringstream(input);//Get a nice cout/cin like stream to parse the input
        
        inputStringStream >> temp;//get the first whitespace seperated token from the input; the command
        
        //todo down the road it would be nice to convert this to a switch statement instead of a horible long if else statement
        //maybe use std::map to map from strings to enums that can be used in a switch
        if (temp == "exit")
            wantsToExit = true;
        else if (temp == "about")
            std::cout << "Not implemented yet. Should probably make about into a separate static function in UserInput." << std::endl;
        else
            std::cout << "Yeah, no. That's not a valid command." << std::endl;
    }
}

/* Private Methods */
/* Static Methods */
int UserInput::tileCodeAt(Board &_board, unsigned int x, unsigned int y)//0 if p1, 1 if p2, 2 if p1 king, 3 if p2 king, 4 if invalid tile, -1 means piece is non existent/dead
{
    //look for invalid tiles and set tile code appropriately, then return
    Board::invalidTilesCoordinatesVector_t invalidTiles = _board.getInvalidTilesVector();
    const unsigned long numOfInvalidTiles = invalidTiles.size();
    for (int i = 0; i < numOfInvalidTiles; ++i)
    {
        if ((invalidTiles[i][0] == x) && (invalidTiles[i][1] == y))//The coordinates of the invalid tile match what we are looking for
            return 4;//invalid tile found, so we return that information
    }
    
    //look for pieces and set tileCode appropriately, then return
    const unsigned int numOfPieces = _board.getNumOfPieces();
    for (int i = 0; i < numOfPieces; ++i)
    {
        if ((_board.getXCoordinate(i) == x) && (_board.getYCoordinate(i) == y))//The coordinates of the piece match what we are looking for
        {
            if (_board.isAlive(i))
            {
                if (_board.isP1Piece(i))
                    return _board.isKing(i) ? 2 : 0;//p1 piece, either king or not
                else
                    return _board.isKing(i) ? 3 : 1;//p2 piece, either king or not
            }
        }
    }
    
    return -1;//no pieces or invalid tiles, so the space is empty
}

/* !Static Methods */

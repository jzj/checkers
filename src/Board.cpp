#include <vector>
#include <string>
#include <fstream>
#include <limits>

#include "Board.h"


Board::Board(unsigned int _numOfPieces, pieceCoordinatesVector_t _pieceCoordinates, pieceAliveVector_t _pieceAlive, pieceKingedVector_t _pieceIsKinged, pieceIsP1Vector_t _pieceIsP1, invalidTilesCoordinatesVector_t _invalidTiles, unsigned int _maxX, unsigned int _maxY)
{
    //Copy parameters into class member variables/vectors
    numOfPieces = _numOfPieces;
    
    pieceCoordinates = pieceCoordinatesVector_t(_pieceCoordinates);
    pieceAlive = pieceAliveVector_t(_pieceAlive);
    pieceIsKinged = pieceKingedVector_t(_pieceIsKinged);
    pieceIsP1 = pieceIsP1Vector_t(_pieceIsP1);
    
    invalidTiles = invalidTilesCoordinatesVector_t(_invalidTiles);
    maxX = _maxX;
    maxY = _maxY;
}



Board::Board(std::string fileName)
{
    fileRead(fileName);//let fileRead handle things
}



unsigned int Board::getXCoordinate(unsigned int pieceNum)
{
    if (pieceNum < numOfPieces)//0 indexed so < instead of <=
        return pieceCoordinates[pieceNum][0];//x coordinate
    else
        return 0; //placeholder todo error handling (invalid argument exception)
}



unsigned int Board::getYCoordinate(unsigned int pieceNum)
{
    if (pieceNum < numOfPieces)//0 indexed so < instead of <=
        return pieceCoordinates[pieceNum][1];//y coordinate
    else
        return 0; //placeholder todo error handling (invalid argument exception)
}



void Board::setXCoordinate(unsigned int pieceNum, unsigned int x)
{//todo ensure coords not out of bounds
    if (pieceNum < numOfPieces)//0 indexed so < instead of <=
        pieceCoordinates[pieceNum][0] = x;//x coordinate
    //else//placeholder todo error handling (invalid argument exception)
}



void Board::setYCoordinate(unsigned int pieceNum, unsigned int y)
{//todo ensure coords not out of bounds
    if (pieceNum < numOfPieces)//0 indexed so < instead of <=
        pieceCoordinates[pieceNum][1] = y;//y coordinate
    //else//placeholder todo error handling (invalid argument exception)
}



bool Board::isAlive(unsigned int pieceNum)
{
    if (pieceNum < numOfPieces)//0 indexed so < instead of <=
        return pieceAlive[pieceNum];
    else
        return false; //placeholder todo error handling (invalid argument exception)
}



void Board::killPiece(unsigned int pieceNum)
{
    if (pieceNum < numOfPieces)//0 indexed so < instead of <=
        pieceAlive[pieceNum] = false;
    //else//placeholder todo error handling (invalid argument exception)
}



void Board::resurrectPiece(unsigned int pieceNum)
{
    if (pieceNum < numOfPieces)//0 indexed so < instead of <=
        pieceAlive[pieceNum] = false;
    //else//placeholder todo error handling (invalid argument exception)
}



bool Board::isKing(unsigned int pieceNum)
{
    if (pieceNum < numOfPieces)//0 indexed so < instead of <=
        return pieceIsKinged[pieceNum];
    else
        return false; //placeholder todo error handling (invalid argument exception)
}



void Board::kingPiece(unsigned int pieceNum)
{
    if (pieceNum < numOfPieces)//0 indexed so < instead of <=
        pieceIsKinged[pieceNum] = true;
    //else//placeholder todo error handling (invalid argument exception)
}



void Board::dethronePiece(unsigned int pieceNum)
{
    if (pieceNum < numOfPieces)//0 indexed so < instead of <=
        pieceIsKinged[pieceNum] = true;
    //else//placeholder todo error handling (invalid argument exception)
}



bool Board::isP1Piece(unsigned int pieceNum)
{
    if (pieceNum < numOfPieces)//0 indexed so < instead of <=
        return pieceIsP1[pieceNum];
    else
        return false; //placeholder todo error handling (invalid argument exception)
}



unsigned int Board::getNumOfPieces()
{
    return numOfPieces;
}



unsigned int Board::getMaxXCoordinate()
{
    return maxX;
}



unsigned int Board::getMaxYCoordinate()
{
    return maxY;
}



void Board::setMaxXCoordinate(unsigned int x)
{
    maxX = x;
}



void Board::setMaxYCoordinate(unsigned int y)
{
    maxY = y;
}



Board::invalidTilesCoordinatesVector_t Board::getInvalidTilesVector()
{
    return Board::invalidTilesCoordinatesVector_t(invalidTiles);//construct a new tiles vector and return it to the caller
}



void Board::fileRead(const std::string &fileName)//todo error handling/file sanity checking
{
    std::ifstream headerStream(fileName);
    char headerChar = headerStream.get();
    if (headerChar == 't')
        templateRead(fileName);
    else if (headerChar == 's')
        saveRead(fileName);
    //else
        //todo error handling
}



void Board::templateWrite(const std::string &fileName)//todo error handling/file sanity checking
{
    //todo
}



void Board::saveWrite(const std::string &fileName)//todo error handling/file sanity checking
{
    //todo
}



void Board::templateRead(const std::string &fileName)//todo error handling/file sanity checking
{
    unsigned int numOfInvalidTiles;//used to create invalidTiles vector
    unsigned int startingPlayerCode;//used to determine isPlayerOneTurn
    unsigned int temp1, temp2;
    
    std::ifstream fileStream(fileName);
    fileStream.seekg(2);//seek to the first number in the header
    
    //Read header
    fileStream >> numOfPieces >> numOfInvalidTiles >> maxX >> maxY >> startingPlayerCode;
    isPlayerOneTurn = startingPlayerCode == 0;//if the starting player is 0 (p1), then load that information
    
    //Go to next line//thanks https://stackoverflow.com/questions/477408/ifstream-end-of-line-and-move-to-next-line
    fileStream.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    
    //Clear/fill vectors in the class
    pieceCoordinates.clear();
    pieceAlive.clear();
    pieceAlive.assign(numOfPieces, true);//every piece is alive in a template
    pieceIsKinged.clear();
    pieceIsP1.clear();
    invalidTiles.clear();
    
    //Read pieces
    if (numOfPieces > 0)
    {
        for (int i = 0; i < numOfPieces; ++i)
        {
            fileStream >> temp1 >> temp2;//get x and y coordinate
            pieceCoordinates.push_back({temp1, temp2});//create an array to be pushed into the vector
            
            fileStream >> temp1;
            pieceIsKinged.push_back(temp1 == 1);
            
            fileStream >> temp1;
            pieceIsP1.push_back(temp1 == 1);
            
            //Go to next line//thanks https://stackoverflow.com/questions/477408/ifstream-end-of-line-and-move-to-next-line
            fileStream.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
    }
    
    //Read invalid tiles
    if (numOfPieces > 0)
    {
        for (int i = 0; i < numOfInvalidTiles; ++i)
        {
            fileStream >> temp1 >> temp2;//get x and y coordinate
            invalidTiles.push_back({temp1, temp2});//create an array to be pushed into the vector
            
            //Go to next line//thanks https://stackoverflow.com/questions/477408/ifstream-end-of-line-and-move-to-next-line
            fileStream.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
    }
}



void Board::saveRead(const std::string &fileName)//todo error handling/file sanity checking
{
    //todo
}

#ifndef USERINPUT_H
#define USERINPUT_H

#include "Board.h"

class UserInput
{
    
public:
    static void printBoardStats(Board &_board);
    static void printBoard(Board &_board);
    
    UserInput(Board &_board);//takes a reference to the board object to use
    void run();//command line prompt
    
private:
    static int tileCodeAt(Board &_board, unsigned int x, unsigned int y);//0 if p1, 1 if p2, 2 if p1 king, 3 if p2 king, -1 means piece is non existent
    Board &board;//Refrence to the board we want to use

};

#endif // USERINPUT_H

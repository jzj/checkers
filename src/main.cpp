#include <iostream>
#include <string>
#include <array>

#include "UserInput.h"
#include "Board.h"

std::array<std::string, 2> splitFileName(std::string fileName);//looks like this needs to be defined before main if main wants to use it

int main()
{
    //test constructing board
    //int numPieces = 1;//Only 1 piece
    //Board::pieceCoordinatesVector_t pieceCoords = {{0,0}};//at coordinates 0,0 at the moment
    //Board::pieceAliveVector_t pieceAlive = {true};//That piece is alive
    //Board::pieceKingedVector_t pieceKinged = {false};//That piece is not kinged
    //Board::pieceIsP1Vector_t pieceIsP1 = {true};//It's player 1's piece

    //Board::invalidTilesCoordinatesVector_t invalidTiles = {{7,7}};//top right corner of board is invalid
    //int maxX = 7, maxY = 7;//standard board size (8 by 8)
    
    //Board board(numPieces, pieceCoords, pieceAlive, pieceKinged, pieceIsP1, invalidTiles, maxX, maxY);
    
    //Load board template from disk
    Board board("templates/standard.btemplate");
    
    //Print Stats
    UserInput::printBoardStats(board);
    
    //Print Board
    UserInput::printBoard(board); 
    
    std::cout << std::endl;
    std::cout << "Split.txt: " << splitFileName("Split.txt")[0] << "|" << splitFileName("Split.txt")[1] << std::endl;
    
    std::cout << "Ok, done initial tests. Moving into UserInput.run()" << std::endl;
    UserInput cli(board);//Pass our best friend mr. Board object to our other best freind mr. UserInput object
    cli.run();//Give execution over to UserInput
    
    return 0;//Nous sommes fini
}

std::array<std::string, 2> splitFileName(std::string fileName)//splitFileName[0] is the name, splitFileName[0] is the ext
{
    unsigned int dotIndex;
    
    for (int i = fileName.size() - 1; i >= 0; --i)
    {
        if (fileName[i] == '.')
            dotIndex = i;
    }//todo error handling if there is no .
    
    std::string name = fileName.substr(0, dotIndex);//before last . in file name
    std::string ext = fileName.substr(dotIndex + 1);//after last . in file name
    
    return std::array<std::string, 2> {name, ext};
}

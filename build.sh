#!/bin/bash

#Compiler Options (change as needed)
#COMPILER="g++"
COMPILER="clang++"

#Flags (note we include the src directory with -I for compile flags and -L for linking in order to automatically see the .h files)

#Debug flags
COMPILEFLAGS="-Wall -fexceptions -Og -g -std=c++11 -Isrc"
LINKERFLAGS="-Lsrc"


#Code (todo parallelism)

#Folder ignored with .gitignore file
mkdir build

#Compile
$COMPILER $COMPILEFLAGS -c src/UserInput.cpp -o build/UserInput.o
$COMPILER $COMPILEFLAGS -c src/GameEngine.cpp -o build/GameEngine.o
$COMPILER $COMPILEFLAGS -c src/Board.cpp -o build/Board.o
$COMPILER $COMPILEFLAGS -c src/main.cpp -o build/main.o

#Link
$COMPILER $LINKERFLAGS -o build/Checkers build/UserInput.o build/GameEngine.o build/Board.o build/main.o 

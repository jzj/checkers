//plan for game mechanics 

Have a "checkerPieces" class with the following properties:
- bool team
- unsigned int x-coord
- unsigned int y-coord
- bool isAlive
- bool isKinged

Since this is standard checkers, there needs to be exactly 24 checkerPieces

For seeing if a space is legal or not, first compare to a big array of legal spaces; 

(A,1) (A,3) (A,5) (A,7)
(B,2) (B,4) (B,6) (B,8)
(C,1) (C,3) (C,5) (C,7)
(D,2) (D,4) (D,6) (D,8)
(E,1) (E,3) (E,5) (E,7)
(F,2) (F,4) (F,6) (F,8)
(G,1) (G,3) (G,5) (G,7)
(H,2) (H,4) (H,6) (H,8)

^ in this case because I'm doing just standard 8x8 checkers

Doing math just letters is stupid, so whenever a co-ordinate is input, just translate from letters to number first
A -> 1, B -> 2, C -> 3, etc...

Moving pieces will be done by first inputting the co-ordinate of what piece you want to move, followed by where you want to move it. If valid, move the piece, if not, give an error and start the process over again.

For jumping the opponent, run a check to see if another piece is behind the piece to be jumped, if so, throw an error, if not, allow the jump.
This is going to get pretty messy once multiple jumps in a single turn is considered.

Order of Code Execution:
- select the piece wanting to be moved, and the requested space to move it to
- reference the "valid space" array mentioned above
- run a check to see if another piece is in the spot you are aiming for
- if there is a single enemy piece between current and desired location, move to desired location and deactivate the enemy piece
